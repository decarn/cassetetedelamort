
public class Point3D {
	
	/**
	 * 
	 * mon constructeur
	 * @param x mon axe x
	 * @param y mon axe y
	 * @param z mon axe z
	 */
	public Point3D(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	/**
	 * mon constructeur initialisateur
	 */
	public Point3D(){
		this (0, 0, 0);
	}
	
	/** translation de x 
	 * @param a est un paramétre tampon
	 * @return le nouveau x calculé
	 */
	public int transX(int a){
		this.x = this.x+a;
		return x;
		
	}
	
	/** translation de Y 
	 * @param a est un paramétre tampon
	 * @return le nouveau y calculé
	 */
	public int transY(int a){
		this.y = this.y+a;
		return y;
	}
		
	/** translation de z 
	 * @param a est un paramétre tampon
	 * @return le nouveau z calculé
	 */
	public int transZ(int a){
		this.z = this.z+a;
		return z;
	}
	
	
	/** 
	 * rotation de 90° autour de X 
	 */
	public void rotationX(){
		int t;
		t = this.z;
		this.z = this.y;
		this.y = -t;
	}
	
	/** 
	 * rotation de 90° autour de Y 
	 */
	public void rotationY(){
		int t;
		t = this.x;
		this.x = this.z;
		this.z = -t;
	}
	
	/** 
	 * rotation de 90° autour de Z 
	 */
	public void rotationZ(){
		int t;
		t = this.y;
		this.y = this.x;
		this.x = -t;
	}
	
	/** 
	 * renvoie les coordonnées de mon cube.
	 * j'ai surchargé toString
	 */
	 public String toString(){
		 return("Coordonnées de x : "+ this.x +"; de y : "+ this.y +"; de z : "+ this.z +";");
	 }
	 
	private int x;
	private int y;
	private int z;
	
	public int getZ() {
		return this.z;
	}
	
	public int getY() {
		return this.y;
	}
	public int getX() {
		return this.x;
	}

}
