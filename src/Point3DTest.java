import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;


public class Point3DTest extends TestCase {
	private Point3D cube;
	/* a ne plus utiliser */
	/*@Before
	public void setUp(){
		cube = new Point3D(5,8,10);
	}
	*/

	@Test
	public void testPoint3D(){
		cube = new Point3D(12,6,15);
		assertEquals(1, cube.getX());
		assertEquals(6, cube.getY());
		assertEquals(15, cube.getZ());
	}
	
	@Test
	public void testTransX() {
		cube = new Point3D(8,9,10);
		assertNotNull(cube.transX(13));
		//assertNotSame(cube.transX(13), cube);
	}
	
	@Test
	public void testTransY() {
		assertNotNull(cube.transY(20));
		//assertNotSame(cube.transY(20), cube);
	}
	
	@Test
	public void testTransZ() {
		assertNotNull(cube.transZ(1));
	//	assertNotSame(cube.transZ(1), cube);
	}
	
	 @Test
	public void testRotationX() {
		cube.rotationX();
		assertEquals(5, cube.getX());
		assertEquals(-10, cube.getY());
		assertEquals(8, cube.getZ());
	}

	 @Test
	public void testRotationY() {
		cube.rotationY();
		assertEquals(8, cube.getY());
		assertEquals(10, cube.getX());
		assertEquals(-5, cube.getZ());
		
	}

	 @Test
	public void testRotationZ() {
		cube.rotationZ();
		assertEquals(10, cube.getZ());
		assertEquals(-8, cube.getX());
		assertEquals(5, cube.getY());
		
	}

}
