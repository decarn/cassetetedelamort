import java.util.ArrayList;
import java.util.Collection;


public class Shape3D  {
	
/**
 * Mon constructeur par defaud, donnant le nombre dinstance de Shape3D
 * Et imvremente mon nombre de pieces
 */
	public Shape3D() {
		this ("piece "+ nbPiece);
		nbPiece ++;
	}
	
	
	/**
	 * Constructeur creant veritablement ma piece
	 * @param iDDepiece parametre prenant mon instance de piece
	 *  */
	public Shape3D(String iDDepiece){
		this.iDPiece = iDDepiece;
		ptPiece = new ArrayList<Point3D>();
	}

	/**
	 * rajoute une instance a la piece dans la collection ptPiece
	 */
	public void addPoint3D(Point3D point) {
		ptPiece.add(point);
	}
	
	
	/**
	 * Fonction pour translation sur X
	 * @param a parametre de translation
	 * @return nouvelles coordonnees de X
	 */
	public void transShapeX(int a) {
		for(Point3D point : ptPiece){
			point.transX(a);
		}

	}
		
	
	/**
	 * Fonction pour translation sur Y
	 * @param a parametre de translation
	 * @return nouvelles coordonnees de Y
	 */
	public void transShapeY(int a) {
		for(Point3D point : ptPiece){
			point.transY(a);
		}
		
	}
	
	
	/**
	 * Fonction pour translation sur Z
	 * @param a parametre de translation
	 * @return nouvelles coordonnees de Z
	 */
	public void transShapeZ(int a) {
		for(Point3D point : ptPiece){
			point.transZ(a);
		}
	}
	
	
	/**
	 * Fonction pour une rotation sur l'axe des X sur la piece
	 * Et donc sur la collection qui compose la piece.
	 */
	public void rotationX() {
		for (Point3D p : ptPiece) {
			p.rotationX();
		}
	}

	/**
	 * Fonction pour une rotation sur l'axe des Y sur la piece
	 * Et donc sur la collection qui compose la piece.
	 */
	public void rotationY() {
		for(Point3D point : ptPiece){
			point.rotationY();	
		}
	}
	
	/**
	 * Fonction pour une rotation sur l'axe des Z sur la piece
	 * Et donc sur la collection qui compose la piece.
	 */
	public void rotationZ() {
		for(Point3D p : ptPiece){
			p.rotationZ();
		}
			
	}
	
	
	/**
	 * Fonction servant a retourner le nom de la piece
	 * @return iDPiece la chaine de caratere correspondante a nos coordonnees
	 */
	public String getIdPiece() {
		iDPiece = ptPiece.toString(); 
		return iDPiece;
	}
	
	/**
	 * Fonction me donnant les references de mes axes
	 * @return les axes X, Y, et Z
	 */
	public String getCoord() {
		String tmp= "";
		for(Point3D point : this.ptPiece){
			tmp += point.toString();	
		}
		return tmp;
	}

	private Collection<Point3D> ptPiece;
	
	private String iDPiece;
	private static int nbPiece = 0;
	
}
